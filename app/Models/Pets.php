<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Pets
 *
 * @property int $id
 * @property string $nome
 * @property string $especie
 * @method static \Illuminate\Database\Eloquent\Builder|Pets newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pets newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pets query()
 * @method static \Illuminate\Database\Eloquent\Builder|Pets whereEspecie($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pets whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pets whereNome($value)
 * @mixin \Eloquent
 */
class Pets extends Model
{
    use HasFactory;

    protected $table = "pets";
    public $timestamps = false;
}
