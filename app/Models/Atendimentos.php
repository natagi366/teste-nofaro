<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Atendimentos
 *
 * @property int $id
 * @property int $pet_id
 * @property string $descricao
 * @property string $data_atendimento
 * @method static \Illuminate\Database\Eloquent\Builder|Atendimentos newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Atendimentos newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Atendimentos query()
 * @method static \Illuminate\Database\Eloquent\Builder|Atendimentos whereDataAtendimento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Atendimentos whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Atendimentos whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Atendimentos wherePetId($value)
 * @mixin \Eloquent
 */
class Atendimentos extends Model
{
    use HasFactory;

    protected $table = "atendimentos";
    public $timestamps = false;
}
