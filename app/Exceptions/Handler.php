<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * @param Request $request
     * @param Throwable $exception
     * @return JsonResponse|Response|\Symfony\Component\HttpFoundation\Response
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof ValidationException) {
            return parent::render($request, $exception);
        }

        $message = $exception->getMessage();

        $response = array('error' => true, 'message' => $message);
        $statusCode = $this->getStatusCode($exception);

        if (config('app.debug')) {
            $response['file'] = $exception->getFile();
            $response['line'] = $exception->getLine();
            $response['code'] = $exception->getCode();
        }

        return response()->json($response, $statusCode);
    }

    /**
     * @param Throwable $exception
     * @return int|mixed
     */
    protected function getStatusCode(Throwable $exception)
    {
        $statusCode = 500;

        if (
            is_callable(array($exception, 'getStatusCode'))
                &&
            $exception->getStatusCode() >= 99
                &&
            $exception->getStatusCode() < 600
        ) {
            $statusCode = $exception->getStatusCode();
        }

        switch (true) {
            case ($exception instanceof \Exception):
                if (
                    is_callable(array($exception, 'getCode'))
                        &&
                    $exception->getCode() >= 99
                        &&
                    $exception->getCode() < 600
                ) {
                    $statusCode = $exception->getCode();
                }
                break;
            default:
                $statusCode = 500;
        }

        return $statusCode;
    }
}
