<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;

class UpdateAtendimentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "id" => ["required", "integer"],
            "pet_id" => ["required", "integer"],
            "descricao" => ["nullable", "string"],
            "data_atendimento" => ["required", "date", "date_format:Y-m-d"]
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $json = [
            "error" => true,
            "message" => $validator->errors()
        ];

        $response = new JsonResponse($json, 400);

        throw (new ValidationException($validator, $response))->status(400);
    }

    public function messages()
    {
        return [
            'id.required' => 'O id do atendimento é obrigatório.',
            'id.integer' => 'Número do id é inválido',

            'pet_id.required' => 'O id do pet é obrigatório.',
            'pet_id.integer' => 'Número do id do pet é inválido',

            'data_atendimento.required' => 'Data de atendimento é obrigatória',
            'data_atendimento.date' => 'Data de atendimento é inválida',
            'data_atendimento.date_format' => 'Data de atendimento deve estar no formato YYYY-MM-DD',
        ];
    }
}
