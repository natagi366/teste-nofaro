<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;

class PetsIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "page" => ["nullable", "integer"],
            "nome" => ["nullable", "string"]
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $json = [
            "error" => true,
            "message" => $validator->errors()
        ];

        $response = new JsonResponse($json, 400);

        throw (new ValidationException($validator, $response))->status(400);
    }

    public function messages()
    {
        return [
            'page.integer' => 'Número da página é inválida',
        ];
    }
}
