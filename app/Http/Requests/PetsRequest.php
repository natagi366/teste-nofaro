<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;

class PetsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nome" => ["required", "min:2", "string"],
            "especie" => ["required", "max:1", "string"]
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $json = [
            "error" => true,
            "message" => $validator->errors()
        ];

        $response = new JsonResponse($json, 400);

        throw (new ValidationException($validator, $response))->status(400);
    }

    public function messages()
    {
        return [
            'nome.required' => 'O nome do pet é obrigatório.',
            'nome.min' => 'O nome do pet deve ter no mínimo dois caracteres.',

            'especie.required' => 'A espécie do pet é obrigatória.',
            'especie.max' => 'A informação da espécie deve ter no máximo 2 caracteres, sendo C (cão) ou G (gato).',
        ];
    }
}
