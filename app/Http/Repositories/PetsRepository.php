<?php

namespace App\Http\Repositories;

use App\Http\Requests\PetsIndexRequest;
use App\Http\Requests\UpdatePetRequest;
use App\Models\Atendimentos;
use App\Models\Pets;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Throwable;

class PetsRepository
{
    /**
     * @param PetsIndexRequest $request
     * @return LengthAwarePaginator
     */
    public function allPets(PetsIndexRequest $request)
    {
        $pets = new Pets();

        if (isset($request->nome) AND !empty($request->nome)) {
            $pets = $pets->where(
                \DB::raw("LOWER(nome)"),
                "LIKE",
                \DB::raw("LOWER('%{$request->nome}%')")
            );
        }

        return $pets->paginate(15);
    }

    /**
     * @param string $nome
     * @param string $especie
     * @return Pets
     */
    public function insert(string $nome, string $especie): Pets
    {
        $pets = new Pets();
        $pets->nome = $nome;
        $pets->especie = $especie;
        $pets->save();

        return $pets;
    }

    /**
     * @param UpdatePetRequest $request
     * @return Pets|Pets[]|Collection|Model|null
     */
    public function update(UpdatePetRequest $request)
    {
        $pet = Pets::findOrFail($request->id);
        $pet->nome = $request->nome;
        $pet->especie = $request->especie;
        $pet->save();

        return $pet;
    }

    /**
     * @param int $id
     * @return Pets|Pets[]|Collection|Model|null
     * @throws Exception|Throwable
     */
    public function delete(int $id)
    {
        try {
            \DB::beginTransaction();

            Atendimentos::where(
                "pet_id",
                "=",
                $id
            )->delete();

            $pet = Pets::where(
                "id",
                "=",
                $id
            )->delete();

            \DB::commit();

            return $pet;
        } catch (\Throwable $throwable) {
            \DB::rollBack();

            throw new \Exception($throwable->getMessage());
        }
    }
}
