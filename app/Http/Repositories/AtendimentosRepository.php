<?php

namespace App\Http\Repositories;

use App\Http\Requests\UpdateAtendimentoRequest;
use App\Models\Atendimentos;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class AtendimentosRepository
{
    /**
     * @param int $pet_id
     * @param string | null $descricao
     * @param string $data_atendimento
     * @return Atendimentos
     */
    public function insert(int $pet_id, $descricao, string $data_atendimento)
    {
        $atendimentos = new Atendimentos();
        $atendimentos->pet_id = $pet_id;
        $atendimentos->descricao = $descricao;
        $atendimentos->data_atendimento = $data_atendimento;
        $atendimentos->save();

        return $atendimentos;
    }

    /**
     * @param UpdateAtendimentoRequest $request
     * @return Atendimentos|Atendimentos[]|Collection|Model|null
     */
    public function update(UpdateAtendimentoRequest $request)
    {
        $atendimento = Atendimentos::findOrFail($request->id);
        $atendimento->pet_id = $request->pet_id;
        $atendimento->descricao = $request->descricao;
        $atendimento->data_atendimento = $request->data_atendimento;
        $atendimento->save();

        return $atendimento;
    }

    /**
     * @param int $id
     * @return Atendimentos|Atendimentos[]|Collection|Model|null
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $atendimento = Atendimentos::findOrFail($id);
        $atendimento->delete();

        return $atendimento;
    }

    /**
     * @param null $id
     * @return Model|Builder|\Illuminate\Support\Collection|object|null
     */
    public function allInformation($id = null)
    {
        $atendimentos = Atendimentos::join(
            "pets",
            "pets.id",
            "=",
            "atendimentos.pet_id"
        )->select([
            "atendimentos.id AS atendimento_id",
            "atendimentos.descricao",
            "atendimentos.data_atendimento",
            "pets.id AS pets_id",
            "pets.nome",
            "pets.especie",
            \DB::raw(
                "CONCAT(
                            'Em ',
                            DATE_FORMAT(atendimentos.data_atendimento, \"%d/%m/%Y\"),
                            ' o pet ',
                            pets.nome,
                            ' ',
                            (CASE WHEN pets.especie = 'C' THEN '(cão)' ELSE '(gato)' END),
                            ' ',
                            atendimentos.descricao
                        ) AS atendimento"
            )
        ]);

        if (!empty($id)) {
            return $atendimentos->where(
                "atendimentos.id",
                "=",
                $id
            )->first();
        }

        return $atendimentos->get();
    }
}
