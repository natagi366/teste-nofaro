<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class PingController extends Controller
{
    /**
     * @return string
     */
    public function ping()
    {
        return "pong";
    }
}
