<?php

namespace App\Http\Controllers;

use App\Http\Repositories\AtendimentosRepository;
use App\Http\Requests\AtendimentosRequest;
use App\Http\Requests\UpdateAtendimentoRequest;
use App\Http\Resources\AtendimentosResource;
use App\Models\Atendimentos;

class AtendimentosController extends Controller
{
    /**
     * @var AtendimentosRepository
     */
    private $atendimentosRepository;

    /**
     * AtendimentosController constructor.
     * @param AtendimentosRepository $atendimentosRepository
     */
    public function __construct(AtendimentosRepository $atendimentosRepository)
    {
        $this->atendimentosRepository = $atendimentosRepository;
    }

    /**
     * Lista todos os atendimentos
     *
     * @return AtendimentosResource
     */
    public function index()
    {
        $atendimentos = $this->atendimentosRepository->allInformation();

        return new AtendimentosResource($atendimentos);
    }

    /**
     * Salva um atendimento
     *
     * @param AtendimentosRequest $request
     * @return AtendimentosResource
     */
    public function store(AtendimentosRequest $request)
    {
        $atendimento = $this->atendimentosRepository->insert(
            $request->pet_id,
            $request->descricao,
            $request->data_atendimento
        );

        return new AtendimentosResource($atendimento);
    }

    /**
     * Busca por um atendimento específico
     *
     * @param  int  $id
     * @return AtendimentosResource
     */
    public function show(int $id)
    {
        $atendimentos = $this->atendimentosRepository->allInformation($id);

        return new AtendimentosResource($atendimentos);
    }

    /**
     * Atualiza um atendimento
     *
     * @param UpdateAtendimentoRequest $request
     * @return AtendimentosResource
     */
    public function update(UpdateAtendimentoRequest $request)
    {
        $atendimento = $this->atendimentosRepository->update($request);

        return new AtendimentosResource($atendimento);
    }

    /**
     * Deleta um atendimento.
     *
     * @param  int  $id
     * @return AtendimentosResource
     */
    public function destroy(int $id)
    {
        $atendimento = $this->atendimentosRepository->delete($id);

        return new AtendimentosResource($atendimento);
    }
}
