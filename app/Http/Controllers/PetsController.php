<?php

namespace App\Http\Controllers;

use App\Http\Repositories\PetsRepository;
use App\Http\Requests\PetsIndexRequest;
use App\Http\Requests\PetsRequest;
use App\Http\Requests\UpdatePetRequest;
use App\Http\Resources\PetsResource;
use App\Models\Pets;
use Throwable;

class PetsController extends Controller
{
    /**
     * @var PetsRepository
     */
    private $petsRepository;

    /**
     * PetsController constructor.
     * @param PetsRepository $petsRepository
     */
    public function __construct(PetsRepository $petsRepository)
    {
        $this->petsRepository = $petsRepository;
    }

    /**
     * Lista todos os pets
     *
     * @param PetsIndexRequest $request
     * @return PetsResource
     */
    public function index(PetsIndexRequest $request)
    {
        $pets = $this->petsRepository->allPets($request);

        return new PetsResource($pets);
    }

    /**
     * Salva um pet
     *
     * @param PetsRequest $request
     * @return PetsResource
     */
    public function store(PetsRequest $request)
    {
        $pet = $this->petsRepository->insert(
            $request->nome,
            $request->especie
        );

        return new PetsResource($pet);
    }

    /**
     * Busca por um pet específico
     *
     * @param  int  $id
     * @return PetsResource
     */
    public function show(int $id)
    {
        return new PetsResource(Pets::find($id));
    }

    /**
     * Atualiza um pet
     *
     * @param UpdatePetRequest $request
     * @return PetsResource
     */
    public function update(UpdatePetRequest $request)
    {
        $pet = $this->petsRepository->update($request);

        return new PetsResource($pet);
    }

    /**
     * Remove um pet
     *
     * @param int $id
     * @return PetsResource
     * @throws Throwable
     */
    public function destroy(int $id)
    {
        $pet = $this->petsRepository->delete($id);

        return new PetsResource($pet);
    }
}
