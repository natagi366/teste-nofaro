<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/ping', 'PingController@ping');

Route::post("/pets", "PetsController@store");
Route::post("/pets/update", "PetsController@update");
Route::get("/pets", "PetsController@index");
Route::get("/pets/{pet}", "PetsController@show");
Route::delete("/pets/{pet}", "PetsController@destroy");

Route::post("/atendimentos", "AtendimentosController@store");
Route::post("/atendimentos/update", "AtendimentosController@update");
Route::get("/atendimentos", "AtendimentosController@index");
Route::get("/atendimentos/{atendimento}", "AtendimentosController@show");
Route::delete("/atendimentos/{atendimento}", "AtendimentosController@destroy");
