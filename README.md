<div style="width: 100%; text-align: center;">
    <h1>Pets</h1>
</div>

<h3>Requisitos Mínimos</h3>

<ul>
  <li>PHP 7.3^</li>
  <li>Extensão PDO MySQL</li>
  <li>Servidor MySQL</li>
</ul>

<hr>

<h3>Banco de Dados</h3>

<h5>Configuração Iniciais</h5>

<ul>
  <li><strong>Criar</strong> uma base de dados com um nome de <strong>sua escolha</strong></li>
</ul>

<h5>API</h5>

<ul>
  <li>Executar no terminal, dentro da pasta do projeto: <strong>cp .env.example .env</strong></li>
  <li>Abra o arquivo <strong>.env</strong> e adicione suas informações de <strong>conexão com o banco de dados</strong></li>
</ul>

<hr>

<h3>API</h3>

<h5>Configurações Iniciais</h5>

<ul>
  <li>
    Executar no terminal, dentro da pasta do projeto:
    <ul>
      <li><strong>php artisan migrate</strong></li>
      <li><strong>php artisan server</strong></li></li>
    </ul>    
  </li>
  <li>Para testar se a API está online <a href="http://localhost:8000/ping"><strong>clique aqui</strong></a></li>
</ul>

<hr>

<h3>Endpoints</h3>

<h5>Pets</h5>

<ul>
  <li>Descrição: Busca os dados de todos os pets cadastrados de forma paginada e com a possibilidade de filtro por nomes parciais.</li>
  <li>Endpoint: <a href="http://localhost:8000/pets">http://localhost:8000/pets</a></li>
  <li>Método: <strong>GET</strong></li>
  <li>
    Parâmetros:
    <ul>
      <li>
        <strong>page</strong>
        <ul>
          <li>Tipo: inteiro</li>
          <li>Endpoint: <a href="http://localhost:8000/pets?page=1">http://localhost:8000/pets?page=1</a></li>
        </ul>
      </li>
      <li>
        <strong>nome:</strong>
        <ul>
          <li>Tipo: string</li>
          <li>Endpoint: <a href="http://localhost:8000/pets?nome=Boli">http://localhost:8000/pets?nome=Boli</a></li>
        </ul>
      </li>
      <li>
        <strong>page e nome:</strong>
        <ul>
          <li>Endpoint: <a href="http://localhost:8000/pets?page=1&nome=Boli">http://localhost:8000/pets?page=1&nome=Boli</a></li>
        </ul>
      </li>
    </ul>
  </li>
  <li>Retorno: Uma coleção de dados dos pets cadastrados</li>
</ul>

<hr>

<ul>
  <li>Descrição: Busca os dados de um pet específico a partir do id</li>
  <li>Endpoint: <a href="http://localhost:8000/pets/1">http://localhost:8000/pets/1</a></li>
  <li>Método: <strong>GET</strong></li>
  <li>
    Parâmetros:
    <ul>
      <li>
        <strong>id</strong>
        <ul>
          <li>Tipo: inteiro</li>
        </ul>
      </li>
    </ul>
  </li>
  <li>Retorno: Um objeto com os dados do pet</li>
</ul>

<hr>

<ul>
  <li>Descrição: Inclui um pet</li>
  <li>Endpoint: <a href="http://localhost:8000/pets">http://localhost:8000/pets</a></li>
  <li>Método: <strong>POST</strong></li>
  <li>
    Parâmetros:
    <ul>
      <li>
        <strong>nome</strong>
        <ul>
          <li>Tipo: string</li>
        </ul>
      </li>
      <li>
        <strong>especie</strong>
        <ul>
          <li>Tipo: string</li>
        </ul>
      </li>
    </ul>
  </li>
  <li>Retorno: Um objeto com os dados do pet incluido</li>
</ul>

<hr>

<ul>
  <li>Descrição: Atualiza um pet</li>
  <li>Endpoint: <a href="http://localhost:8000/pets/update">http://localhost:8000/pets/update</a></li>
  <li>Método: <strong>POST</strong></li>
  <li>
    Parâmetros:
    <ul>
      <li>
        <strong>id</strong>
        <ul>
          <li>Tipo: inteiro</li>
        </ul>
      </li>
      <li>
        <strong>nome</strong>
        <ul>
          <li>Tipo: string</li>
        </ul>
      </li>
      <li>
        <strong>especie</strong>
        <ul>
          <li>Tipo: string</li>
        </ul>
      </li>
    </ul>
  </li>
  <li>Retorno: Um objeto com os dados do pet atualizado</li>
</ul>

<hr>

<ul>
  <li>Descrição: Deleta o pet e seus atendimetos</li>
  <li>Endpoint: <a href="http://localhost:8000/pets/1">http://localhost:8000/pets/1</a></li>
  <li>Método: <strong>DELETE</strong></li>
  <li>
    Parâmetros:
    <ul>
      <li>
        <strong>id</strong>
        <ul>
          <li>Tipo: inteiro</li>
        </ul>
      </li>
    </ul>
  </li>
  <li>Retorno: Um objeto com os dados do pet deletado</li>
</ul>

<h5>Atendimentos</h5>

<ul>
  <li>Descrição: Busca os dados de todos os atendimentos cadastrados.</li>
  <li>Endpoint: <a href="http://localhost:8000/atendimentos">http://localhost:8000/atendimentos</a></li>
  <li>Método: <strong>GET</strong></li>
  <li>Retorno: Uma coleção de dados dos atendimentos cadastrados</li>
</ul>

<hr>

<ul>
  <li>Descrição: Busca os dados de um atendimento específico a partir do id</li>
  <li>Endpoint: <a href="http://localhost:8000/atendimentos/1">http://localhost:8000/atendimentos/1</a></li>
  <li>Método: <strong>GET</strong></li>
  <li>
    Parâmetros:
    <ul>
      <li>
        <strong>id</strong>
        <ul>
          <li>Tipo: inteiro</li>
        </ul>
      </li>
    </ul>
  </li>
  <li>Retorno: Um objeto com os dados do atendimento</li>
</ul>

<hr>

<ul>
  <li>Descrição: Inclui um atendimento</li>
  <li>Endpoint: <a href="http://localhost:8000/atendimentos">http://localhost:8000/atendimentos</a></li>
  <li>Método: <strong>POST</strong></li>
  <li>
    Parâmetros:
    <ul>
      <li>
        <strong>pet_id</strong>
        <ul>
          <li>Tipo: inteiro</li>
        </ul>
      </li>
      <li>
        <strong>descricao</strong>
        <ul>
          <li>Tipo: string</li>
          <li>Opcional: Sim</li>
        </ul>
      </li>
      <li>
        <strong>data_atendimento</strong>
        <ul>
          <li>Tipo: date | string</li>
          <li>Formato: YYYY-MM-DD</li>
        </ul>
      </li>
    </ul>
  </li>
  <li>Retorno: Um objeto com os dados do atendimento incluido</li>
</ul>

<hr>

<ul>
  <li>Descrição: Atualiza um atendimento</li>
  <li>Endpoint: <a href="http://localhost:8000/atendimentos/update">http://localhost:8000/atendimentos/update</a></li>
  <li>Método: <strong>POST</strong></li>
  <li>
    Parâmetros:
    <ul>
      <li>
        <strong>id</strong>
        <ul>
          <li>Tipo: inteiro</li>
        </ul>
      </li>
      <li>
        <strong>pet_id</strong>
        <ul>
          <li>Tipo: inteiro</li>
        </ul>
      </li>
      <li>
        <strong>descricao</strong>
        <ul>
          <li>Tipo: string</li>
          <li>Opcional: Sim</li>
        </ul>
      </li>
      <li>
        <strong>data_atendimento</strong>
        <ul>
          <li>Tipo: date | string</li>
          <li>Formato: YYYY-MM-DD</li>
        </ul>
      </li>
    </ul>
  </li>
  <li>Retorno: Um objeto com os dados do atendimento atualizado</li>
</ul>

<hr>

<ul>
  <li>Descrição: Deleta um atendimento</li>
  <li>Endpoint: <a href="http://localhost:8000/atendimentos/1">http://localhost:8000/atendimentos/1</a></li>
  <li>Método: <strong>DELETE</strong></li>
  <li>
    Parâmetros:
    <ul>
      <li>
        <strong>id</strong>
        <ul>
          <li>Tipo: inteiro</li>
        </ul>
      </li>
    </ul>
  </li>
  <li>Retorno: Um objeto com os dados do atendimento deletado</li>
</ul>
