<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CriaEstruturaBase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("pets", function (Blueprint $table) {
            $table->integer("id")->autoIncrement();
            $table->string('nome');
            $table->char('especie', 1);
        });

        Schema::create("atendimentos", function (Blueprint $table) {
            $table->integer("id")->autoIncrement();
            $table->integer('pet_id');
            $table->string('descricao')->nullable();
            $table->date('data_atendimento');
            $table->foreign('pet_id')->references('id')->on('teste_nofaro.pets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atendimentos');
        Schema::dropIfExists('pets');
    }
}
